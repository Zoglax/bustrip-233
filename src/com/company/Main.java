package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N,k=0, height, i=1;

        N=scanner.nextInt();
        while (i<=N)
        {
            height=scanner.nextInt();
            if(height<=437 && k==0)
            {
                k=i;
            }
            i++;
        }
        if (k==0)
        {
            System.out.println("No crash");
        }
        else
        {
            System.out.println("Crash "+k);
        }
    }
}
